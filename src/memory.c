#include "include/memory.h"
#include "include/font.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

memory_t* memory_init(void)
{
    memory_t *mem = malloc(sizeof memory_t);
    if (!mem)
    {
        printf("Cannot allocate %d bytes for a new memory module\n",
                sizeof memory_t);
        return NULL;
    }

    memory_clear(mem);
    memcpy(mem->mem.fonts, fonts, sizeof fonts);
    return mem;
}

void memory_clear(memory_t* mem)
{
    memset(mem->mem.raw, CLEAR_MEM_SENTINEL, sizeof *mem);
}

void memory_write(memory_t *mem, uint16_t address, uint16_t data)
{
    if (address >= 0x0200 && address < 0x1000)
    {
        mem->mem.raw[address]       = (data & 0xFF00) >> 8;
        mem->mem.raw[address + 1]   = (data & 0x00FF);
    }
}

uint16_t memory_read(memory_t *mem, uint16_t address)
{
    if (address >= 0x0200 && address < 0x1000)
    {
        return (uint16_t) (mem->mem.raw[address] << 8 | mem->mem.raw[address + 1]);
    }
    else
    {
        return CLEAR_MEM_SENTINEL;
    }
}

