#ifndef CHIP8_EMULATOR_CONSTANTS_H
#define CHIP8_EMULATOR_CONSTANTS_H 1

static const char PROGRAM_NAME[] = "Chip 8 emulator";

#define MEMORY_SIZE         (1024 * 4) // 4KB
#define INTERPRETER_SIZE    (512)     // 512 kb: 0 to 511
#define ENTRY_POINT_OFFSET  (0x200)
#define NUM_CPU_REGISTERS   (16) // 16 registers - V0 to VF
#define FLAG_REGISTER       (0x0F) // register used as flag for some instructions
#define NUM_INPUT_KEYS      (16) // input keys: 0 to F

#define SCREEN_WIDTH        (64)
#define SCREEN_HEIGHT       (32)

#endif

