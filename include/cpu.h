#ifndef CHIP8_EMULATOR_CPU_H
#define CHIP8_EMULATOR_CPU_H 1

#include <stdint.h>

#include "include/constants.h"
#include "include/memory.h"

#define CPU_FREQUENCY_HZ    (60)
#define STACK_SIZE          (16)

typedef struct CPU
{
    uint8_t v[NUM_CPU_REGISTERS]; // general purpose registers
    uint16_t I, pc, *sp; // special registers
    uint16_t stack[STACK_SIZE];
    uint16_t DT, ST; // delay and sound timers

    memory_t *memory_bus;
} cpu_t;

cpu_t*      cpu_init(void);
void        cpu_reset(cpu_t *cpu);

uint16_t    cpu_fetch(cpu_t *cpu);
uint16_t    cpu_decode(cpu_t *cpu);
uint16_t    cpu_execute(cpu_t *cpu);

uint16_t    cpu_tick_delay(cpu_t *cpu);
uint16_t    cpu_tick_sound(cpu_t *cpu);


#endif // CHIP8_EMULATOR_CPU_HCHIP8_EMULATOR_CPU_H

