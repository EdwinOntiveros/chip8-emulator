#ifndef CHIP8_EMULATOR_MEMORY_H
#define CHIP8_EMULATOR_MEMORY_H 1

#include <stdint.h>
#include <stdbool.h>

#include "include/constants.h"
#include "include/font.h"

#define CLEAR_MEM_SENTINEL (0xFFFF)

typedef struct MEMORY
{
    union {
        union {
            uint8_t fonts[FONT_MEMORY_SIZE];
            uint8_t interpreter[INTERPRETER_SIZE - FONT_MEMORY_SIZE];
            uint8_t ram[MEMORY_SIZE - INTERPRETER_SIZE];
        };
        uint8_t raw[MEMORY_SIZE];
    } mem;
} memory_t;

void        memory_write(uint16_t address, uint16_t data);
uint16_t    memory_read(uint16_t address);
void        memory_clear(memory_t* mem);
memory_t*   memory_init(void);

#endif // CHIP8_EMULATOR_MEMORY_H
